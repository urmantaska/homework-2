#include <iostream>
#include <stdint.h>
#include <iomanip>

int main()
{
	std::string first = "100509";
	std::cout << first << "\n";
	std::cout << first.length() << "\n";
	std::cout << first[0] << "\n";
	std::cout << first[first.length()-1] << "\n";
	return 0;
}